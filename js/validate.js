		function validate() {

			
			var nome = formReg.nome.value;
			var data = formReg.data.value;
			var horai = formReg.horai.value;
			var horaf = formReg.horaf.value;
			var localizacao = formReg.localizacao.value;
			var descricao = formReg.descricao.value;
			var ingresso = formReg.ingresso.value;
			var categoria = formReg.categoria[].value;
			var estrutura = formReg.estrutura[].value;

			if (nome.value == "" || nome.value <= 5) {
				alert('Preencha o campo nome.');
				formReg.nome.focus();
				return false;
			}
			if (data == "") {
				alert('Preencha o campo data.')
				formReg.data.focus();
				return false;
			}
			if (horai == "") {
				alert('Preencha o campo hora inicial.')
				formReg.horai.focus();
				return false;
			}
			if (horaf == "") {
				alert('Preencha o campo hora final.');
				formReg.horaf.focus();
				return false;
			}
			if (localizacao == "") {
				alert('Preencha o campo localizacao.');
				formReg.localizacao.focus();
				return false;
			}
			if (descricao == "") {
				alert('Preencha o campo descricao.');
				formReg.descricao.focus();
				return false;
			}
			if (ingresso == "") {
				alert('Preencha o campo ingresso.');
				formReg.ingresso.focus();
				return false;
			}
			if (categoria == "") {
				alert('Preencha o campo categoria.');
				formReg.categoria.focus();
				return false;
			}
			if (estrutura == "") {
				alert('Preencha o campo estrutura.');
				formReg.estrutura.focus();
				return false;
			}
			

		}

$("#formReg").validate();


