<?php 
include 'cabecalho.php';
include 'conexao.php';
include 'funcoes-produto.php';

$id = $_POST['id'];
$nome = $_POST['nome'];
$preco = $_POST['preco'];
$descricao = $_POST['descricao'];
$arquivo = $_FILES['arquivo']['name'];
$arquivo_temp = $_FILES['arquivo']['tmp_name'];
$tamanho_arquivo = $_FILES['arquivo']['size'];
$tipo_arquivo = $_FILES['arquivo']['type'];

if(alteraProduto ($conexao, $id, $nome, $preco, $descricao, $arquivo)) { ?>
	<p>O produto <?=$nome?> foi alterado</p>

<?php } else { ?>
	<p> O produto <?=$nome?> não foi alterado </p>
<?php } ?>

<?php include 'rodape.php';

?>
