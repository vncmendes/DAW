<?php
require_once 'cabecalho.php';
require_once 'funcoes-evento.php';
require_once 'funcoes-categoria.php';
require_once 'funcoes-usuario.php'; ?>


<div class="container">
	<div class="row" id="divmain">
		
		<?php 
		mostraAlerta("success");
		mostraAlerta("danger");
		

verificaUsuario();

$categorias = listaCategoria($conexao);
$estruturas = listaEstrutura($conexao); ?>
<div id="conteudo" class="col-12">
	<h1 class="py-5">Adicionando Evento</h1>
	<form action="recebe-evento.php" method="POST" name="formReg" id="formId" enctype="multipart/form-data">
	<table class="mx-5 py-5">
	<tr>
		<td>Nome do Evento:</td>
		<td><input class="form-control" type="text" name="nome"></td>
	</tr>
	<tr>
		<td>Data:</td>
		<td><input class="form-control" type="date" name="data"></td>
	</tr>
	<tr>
		<td>Hora Inicial:</td>
		<td><input class="form-control" type="time" name="horai"></td>
	</tr>
	<tr>
		<td>Hora Final:</td>
		<td><input class="form-control" type="time" name="horaf"></td>
	</tr>
	<tr>
		<td>Localização:</td>
		<td><input class="form-control" type="text" name="localizacao"></td>
	</tr>
	<tr>
		<td>Descrição:</td>
		<td> <textarea class="form-control" name="descricao"></textarea></td>
	</tr>
	<tr>
		<td>Guarda-Volumes</td>
		<td><input  type="radio" value="1"  name="gv">Sim</td>
		<td><input type="radio" value="0"  name="gv" checked>Não</td>
	</tr>
	<tr>
		<td>Ingresso:</td>
		<td><input class="form-control" type="number" name="ingresso"></td>
	</tr>



		<tr>
			<td id="ce">Categoria<br>

			
				
				<?php foreach ($categorias as $categoria) : ?> 
				<input type="checkbox" name="categoria[]" value="<?=$categoria['idcategoria']?>"><?=$categoria['nome']?><br>
				<?php endforeach; ?>
			</td>	
			<td>
				
			</td>

		
			<td>Estrutura<br>
			
				<?php foreach ($estruturas as $estrutura) : ?>
					<input type="checkbox" name="estrutura[]" value="<?=$estrutura['idestrutura']?>"><?=$estrutura['nome']?><br>
				<?php endforeach; ?>
			</td>

		
		</tr>
			<td>Foto:</td>
			<td><input type="file" name="arquivo"></td>
		
			<input type="hidden" name="max_file_size" value="200000">
			<td><input class="btn btn-success" type="submit" onsubmit="return validate();" value="Submeter"></td>
			<td><a class="btn btn-primary" href="index.php">Voltar</a></td>
		
			
		</table>
	</form>
</div>

		</div>
	</div>
	
	

</body>
<?php require_once 'rodape.php'; ?>
</html>