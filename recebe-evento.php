<?php 

require_once 'funcoes-evento.php';
require_once 'config-upload.php'; 
require_once 'funcoes-usuario.php';
require_once 'cabecalho.php';

verificaUsuario();

$varnome = false;
$vardata = false;
$varhorai = false;
$varhoraf = false;
$varlocaliza = false;
$vardescri = false;
$varing = false;
$varcat = false;
$varestru = false;

$nome = $_POST['nome'];
$data = $_POST['data'];
$horainicial = $_POST['horai'];
$horafinal = $_POST['horaf'];
$localizacao = $_POST['localizacao'];
$descricao = $_POST['descricao'];
$gv = $_POST['gv'];
$ingresso = $_POST['ingresso'];
$categoria = $_POST['categoria'];
$estrutura = $_POST['estrutura'];
$arquivo = $_FILES['arquivo']['name'];
$arquivo_temp = $_FILES['arquivo']['tmp_name'];
$tamanho_arquivo = $_FILES['arquivo']['size'];
$tipo_arquivo = $_FILES['arquivo']['type'];



if ($nome == "" || $data == "" || $horainicial == "" || $horafinal == "" || $localizacao == "" || $descricao == "" || $ingresso == "" || $categoria == "" || $estrutura == "") {
	$_SESSION["danger"] = "Preencha todos os campos";
	header("Location: adiciona-evento.php");
}
else {
	$varnome = true;
	$vardata = true;
	$varhorai = true;
	$varhoraf = true;
	$varlocaliza = true;
	$vardescri = true;
	$varing = true;
	$varcat = true;
	$varestru = true;
}

if ($varnome = true && $vardata = true && $varhorai = true && $varhoraf = true && $varlocaliza = true && $vardescri = true && $varing = true && $varcat = true && $varestru = true) {

if (insereEvento ($conexao, $nome, $data, $horainicial, $horafinal, $localizacao, $descricao, $gv, $ingresso, $arquivo, $categoria, $estrutura)) { ?>
	<p class="text-success">O evento <?= $nome ?> foi inserido com Sucesso</p>
<?php }  

else { 
	$msg = mysqli_error($conexao); ?> 
	<p class="text-danger">O evento <?= $nome ?> nao foi Adicionado. <?= $msg ?> </p> <?php
}

if ($sobrescrever == "não" && file_exists("imagens/$arquivo"))
	die("Arquivo já existe");

if ($limitar_tamanho =="não" && ($tamanho_arquivo > $tamanho_bytes))
	die("Arquivo deve ter no máximo $tamanho_bytes bytes");

$ext = strrchr($arquivo, '.');

if (($limitar_ext == "sim") && !in_array ($ext, $extensoes_validas))
	die("Extensão de arquivo inválida para upload");


if (!empty($arquivo)) {
	if (move_uploaded_file($arquivo_temp, "imagens/$arquivo")) {
		echo "Upload do arquivo: ".$arquivo. " conclúido com sucesso";
	}
	else {
		die("Falha no Erro");
	}
	
}
	else {
		die("Selecione o arquivo a ser enviado");
	}
}


require_once 'rodape.php'; ?>