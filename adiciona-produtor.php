<?php
require_once 'cabecalho.php';  
require_once 'funcoes-usuario.php';
require_once 'funcoes-produtor.php'; ?>


<div class="container">

	

<div id="conteudo" class="col-12">
	<h1 class="py-5">Cadastro do Produtor</h1>
	<form action="recebe-produtor.php" method="POST" name="formProd" id="formProd" enctype="multipart/form-data">
	<table class="mx-5 py-5">
	<tr>
		<td>Nome:</td>
		<td><input class="form-control" type="text" name="nome"></td>
	</tr>
	<tr>
		<td>Sobrenome:</td>
		<td><input class="form-control" type="text" name="sobrenome"></td>
	</tr>
	<tr>
		<td>E-mail:</td>
		<td><input class="form-control" type="email" name="email"></td>
	</tr>
	<tr>
		<td>CEP:</td>
		<td><input class="form-control" type="cep" name="cep"></td>
	</tr>
	<tr>
		<td>CNPJ:</td>
		<td><input class="form-control" type="text" name="cnpj"></td>
	</tr>
	<tr>
		<td>CPF:</td>
		<td> <input class="form-control" type="text" name="cpf"></td>
	</tr>	
	<tr>
		<td>Senha:</td>
		<td> <input class="form-control" type="password" name="senha"></td>
	</tr>
		<tr>
		<td>Confirmar Senha:</td>
		<td> <input class="form-control" type="password" name="senha2"></td>
	</tr>
	<tr>
		<td>Genêro</td>
		<td><input  type="radio" value="0"  name="gnr">Masculino</td>
		<td><input type="radio" value="1"  name="gnr" checked>Feminino</td>
		<td><input type="radio" value="2"  name="gnr" checked>Outro</td>
	</tr>
			
			<td>Foto:</td>
			<td><input type="file" name="arquivo"></td>
		
			<input type="hidden" name="max_file_size" value="200000">
			<td><input class="btn btn-success" type="submit" onsubmit="return validate();" value="Submeter"></td>
			<td><a class="btn btn-primary" href="index.php">Voltar</a></td>
		
			
		</table>
	</form>
</div>

		</div>
	</div>
	
	

</body>
<?php require_once 'rodape.php'; ?>
</html>