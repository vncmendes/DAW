<?php
require_once 'conexao.php';

function listaCategoria($conexao) {
	$categorias = array();
	$query = "select * from categoria";
	$resultado = mysqli_query($conexao, $query);
	while($categoria = mysqli_fetch_assoc($resultado)) {
		array_push($categorias, $categoria);
	}
	return $categorias;

}

function listaEstrutura($conexao) {
	$estruturas = array();
	$query = "select * from estrutura";
	$resultado = mysqli_query($conexao, $query);
	while($estrutura = mysqli_fetch_assoc($resultado)) {
		array_push($estruturas, $estrutura);
	}
	return $estruturas;

}