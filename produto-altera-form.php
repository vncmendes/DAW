<?php 
include 'conexao.php';
include 'funcoes-produto.php';
include 'cabecalho.php';

$id = $_GET['id'];
$produto = buscaProduto($conexao, $id);

?>

<h1>Alterando Produto</h1>
<form action="altera-produto.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?=$produto['id']?>">
	<table>

	<tr>
		<td>Nome:</td>
		<td><input type="text" name="nome" value="<?=$produto['nome']?>"></td>
	</tr>

	<tr>
		<td>Preço:</td>
		<td><input type="number" name="preco" value="<?=$produto['preco']?>"></td>
	</tr>

	<tr>
		<td>Descrição:</td>
		<td> <textarea name="descricao"><?=$produto['descricao']?></textarea></td>
	</tr>	

	<td>Arquivo:</td>
	<td><input type="hidden" name="max_file_size" value="200000"></td>
	<td><input type="file" name="arquivo"><?=$produto['arquivo']?></td>


	<td><button type="submit">Alterar</button></td>
		</tr>

	</table> 
	</form>

<?php include 'rodape.php';


