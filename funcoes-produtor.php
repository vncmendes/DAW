<?php
require_once 'conexao.php';
session_start();


function buscaAdm ($conexao, $email, $senha) {
	$senhaSha1 = sha1($senha);
	$email = mysqli_real_escape_string($conexao, $email);
	$query = "select * from useradm where email = '{$email}' and senha = '{$senhaSha1}'";
	$resultado = mysqli_query($conexao, $query);
	$adm = mysqli_fetch_assoc($resultado);
	return $adm;
}

function inserirAdm ($conexao, $nome, $email, $senha) {
	$senhaSha1 = sha1($senha);
	$query = "insert into useradm (nome, email, senha) values ('{$nome}', '{$email}', '{$senhaSha1}')";
	$resultado = mysqli_query($conexao, $query);

	return $resultado;
}


function estaLogadoAdm() {
	return isset($_SESSION["adm_logado"]);
}

function verificaAdm () {
	if (!estaLogado()) {
		$_SESSION["danger"] = "Acesso Negado !"; // pode ficar na mesma sessão do usuário comum???????
	header("Location: index.php?falhaSeg=true");
	die();
	}
}


function AdmLogado() {
	return $_SESSION["adm_logado"];
}

function logaAdm($email) {
	$_SESSION["adm_logado"] = $email;
}

function logOutAdm() {
	session_destroy();
	session_start();
}

