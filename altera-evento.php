<?php 
include 'cabecalho.php';
include 'conexao.php';
include 'funcoes-evento.php';
$id = $_POST['id'];
$nome = $_POST['nome'];
$localizacao = $_POST['localizacao'];
$descricao = $_POST['descricao'];
$gv = $_POST['gv'];
$ingresso = $_POST['ingresso'];
$categoria = $_POST['categoria'];
$arquivo = $_FILES['arquivo']['name'];
$arquivo_temp = $_FILES['arquivo']['tmp_name'];
$tamanho_arquivo = $_FILES['arquivo']['size'];
$tipo_arquivo = $_FILES['arquivo']['type'];

if(alteraEvento ($conexao, $id, $nome, $localizacao, $descricao, $gv, $ingresso, $categoria, $arquivo)) { ?>
	<p class="text-success" style="text-align: center;">O evento <?=$nome?> foi alterado</p>

<?php } else { ?>
	<p> O evento <?=$nome?> não foi alterado </p>
<?php } ?>

<?php include 'rodape.php';

?>
