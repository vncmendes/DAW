<?php 
error_reporting(E_ALL ^ E_NOTICE);
include 'mostra-alerta.php'; ?>

<!DOCTYPE html>
<html>
<head>


    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script>
    <script src="js/validar.js"></script>
    <!-- <script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script> -->
    <!-- <script src="http://code.jquery.com/jquery-1.11.1.js"></script> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> -->
    
   
    <title>DiRolê</title>
</head>
<body>


    <nav  class="navbar navbar-expand-lg navbar-dark bg-primary navbar-fixed-top">
        <div class="container">
            <a class="navbar-brand mb-0" class="a0"  href="index.php">DiRolê</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
              <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSite">
                <ul class="nav navbar-nav mr-auto">
              
                    <li class="navbar-item"><a class="nav-link" class="a1" href="adiciona-evento.php"> Adiciona Evento</a></li>
                    <li class="navbar-item"><a class="nav-link" class="a2" href="evento-lista.php"> Lista de Eventos</a></li>
                    <li class="navbar-item"><a class="nav-link" class="a3" href="contato.php">Contato</a></li>
                   
                    <li class="navbar-item"><a class="nav-link" class="a4" id="register" onclick="mostrarModal1()">Registrar</a></li>
                    <li class="navbar-item"><a class="nav-link" class="a5" id="login" onclick="mostrarModal()">Login</a></li>
                    
                </ul>

                <form class="form-inline">
               		<input class="form-control ml-4 mr-3" class="a6" type="search" placeholder="Buscar...">
               		<button class="btn btn-dark" class="7" type="submit">OK</button>
                </form>

                <!-- <ul class="navbar-nav ml-auto">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navDrop">
                      Redes Sociais
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">Facebook</a>
                        <a href="#" class="dropdown-item">Twitter</a>
                        <a href="#" class="dropdown-item">Instagram</a>
                    </div>
                  </li>
                </ul> -->
            </div>
        </div>
    </nav>
	
<div class="" id="divmain">
		
		<?php 
		mostraAlerta("success");
		mostraAlerta("danger");
		?>
		
