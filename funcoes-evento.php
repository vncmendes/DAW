<?php
require_once 'conexao.php';

function insereEvento ($conexao, $nome, $data, $horainicial, $horafinal, $localizacao, $descricao, $gv, $ingresso, $arquivo,$categoria, $estrutura) {
	$query = "insert into eventos (nome, data, horainicial, horafinal, localizacao, descricao, gv, ingresso, arquivo) values ('{$nome}', '{$data}', '{$horainicial}', '{$horafinal}', '{$localizacao}', '{$descricao}', '{$gv}', '{$ingresso}', '{$arquivo}')";

	$result=mysqli_query ($conexao, $query);

	$query1 = "select last_insert_id()";
	$result1 = mysqli_query ($conexao, $query1);
	$r = mysqli_fetch_assoc($result1);

	$idevento = $r['last_insert_id()'];

	 foreach ($categoria as $codigo) {
		$query2= "insert into registra_eventocategoria (idcategoria, idevento) values ('{$codigo}', '{$idevento}')";
			 $result2=mysqli_query ($conexao, $query2);
	 }


	 foreach ($estrutura as $codigo) {
		$query4= "insert into registra_eventoestrutura (idestrutura, idevento) values ('{$codigo}', '{$idevento}')";
			 $result4=mysqli_query ($conexao, $query4);
	 }
	 
	 return $result;
}

function alteraEvento ($conexao, $id, $nome, $localizacao, $descricao, $gv, $ingresso, $categoria, $arquivo) {
	$query = "update eventos set nome = '{$nome}', localizacao = '{$localizacao}', descricao = '{$descricao}', gv = '{$gv}', ingresso = '{$ingresso}', arquivo = '{$arquivo}' where idevento = {$id}";
	
	$result=mysqli_query ($conexao, $query);

$query2 = "delete from registra_eventocategoria where idevento = {$id}";
	
	mysqli_query($conexao, $query2);

		foreach ($categoria as $codigo) {
		$query3= "insert into registra_eventocategoria (idcategoria, idevento) values ('{$codigo}', '{$id}')";
		$result2=mysqli_query ($conexao, $query3);
	 }

	 return $result;

}

// function removeEvento ($conexao, $id) {
// 	$query = "delete from eventos where idevento = {$id}";
// 	echo $query;
// 	return mysqli_query($conexao, $query);
// }

function removeEvento ($conexao, $id) {
	$query = "delete from eventos where idevento = {$id}";
	echo $query;
	return mysqli_query($conexao, $query);
}

function buscaEvento ($conexao, $id) {
	$query = "select * from eventos where idevento = {$id}";
	$resultado =  mysqli_query($conexao, $query);
	return mysqli_fetch_assoc($resultado);
}

function listaEvento($conexao) {
	$eventos = array();
	$resultado = mysqli_query ($conexao, "select * from eventos");
	while($evento = mysqli_fetch_assoc($resultado)) {
			array_push($eventos, $evento);
	}
	
	return $eventos;
}

function listaEventoCategoria($conexao, $idevento) {
	$registra_eventocategorias = array();
	$resultado = mysqli_query ($conexao, "select * from registra_eventocategoria join categoria using (idcategoria) where idevento = $idevento");
	while($registra_eventocategoria = mysqli_fetch_assoc($resultado)) {
			array_push($registra_eventocategorias, $registra_eventocategoria);
	}
	
	return $registra_eventocategorias;
}


function listaEC($conexao, $idevento) {
	$registra_eventocategorias = array();
	$resultado = mysqli_query ($conexao, "select categoria.idcategoria from registra_eventocategoria join categoria using (idcategoria) where idevento = $idevento");
	while($registra_eventocategoria = mysqli_fetch_assoc($resultado)) {
			array_push($registra_eventocategorias, $registra_eventocategoria['idcategoria']);
	}
	
	return $registra_eventocategorias;
}

function listaEE($conexao, $idevento) {
	$registra_eventoestruturas = array();
	$resultado = mysqli_query ($conexao, "select estrutura.idestrutura from registra_eventoestrutura join estrutura using (idestrutura) where idevento = $idevento");
	while($registra_eventoestrutura = mysqli_fetch_assoc($resultado)) {
			array_push($registra_eventoestruturas, $registra_eventoestrutura['idestrutura']);
	}
	
	return $registra_eventoestruturas;
}

function listaEventoEstrutura($conexao, $idevento) {
	$registra_eventoestruturas = array();
	$resultado = mysqli_query ($conexao, "select * from registra_eventoestrutura join estrutura using (idestrutura) where idevento = $idevento");
	while($registra_eventoestrutura = mysqli_fetch_assoc($resultado)) {
			array_push($registra_eventoestruturas, $registra_eventoestrutura);
	}
	
	return $registra_eventoestruturas;
}


?>