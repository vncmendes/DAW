<?php

include 'conexao.php'; 
include 'funcoes-evento.php';
include 'funcoes-usuario.php';

verificaUsuario();
$id = $_POST['id'];
removeEvento($conexao, $id);
$_SESSION["success"] = "Evento Removido com Sucesso";
header("Location: evento-lista.php");
die();
