<?php 
include 'conexao.php';
include 'funcoes-produto.php';
include 'config-upload.php'; 
include 'cabecalho.php';


$nome = $_POST['nome'];
$preco = $_POST['preco'];
$descricao = $_POST['descricao'];
$arquivo = $_FILES['arquivo']['name'];
$arquivo_temp = $_FILES['arquivo']['tmp_name'];
$tamanho_arquivo = $_FILES['arquivo']['size'];
$tipo_arquivo = $_FILES['arquivo']['type'];


if (insereProduto ($conexao, $nome, $preco, $descricao, $arquivo)) { ?>
	<p>O Produto <?= $nome ?> foi inserido com Sucesso</p> <?php 
}

else { ?> <p>O Produto <?= $nome ?> nao foi Adicionado </p> <?php
}

if ($sobrescrever == "não" && file_exists("imagens/$arquivo"))
	die("Arquivo já existe");

if ($limitar_tamanho =="não" && ($tamanho_arquivo > $tamanho_bytes))
	die("Arquivo deve ter no máximo $tamanho_bytes bytes");

$ext = strrchr($arquivo, '.');

if (($limitar_ext == "sim") && !in_array ($ext, $extensoes_validas))
	die("Extensão de arquivo inválida para upload");


if (!empty($arquivo)) {
	if (move_uploaded_file($arquivo_temp, "imagens/$arquivo")) {
		echo "Upload do arquivo: ".$arquivo. " conclúido com sucesso";
	}
	else {
		die("Falha no Erro");
	}
	
}
	else {
		die("Selecione o arquivo a ser enviado");
	}


include 'rodape.php'; ?>