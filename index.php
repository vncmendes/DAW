<?php
error_reporting(E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    
    <script src="http://jqueryvalidation.org/files/dist/jquery.validate.js"></script>
    <script src="js/validar.js"></script>
    <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
    
   
    
   
    <title>DiRolê</title>
</head>
<body>

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->

    <nav  class="navbar navbar-expand-lg navbar-dark bg-primary navbar-fixed-top">
        <div class="container">
            <a class="navbar-brand mb-0 display-5" class="a0"  href="index.php">DiRolê</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
              <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse" id="navbarSite">
                <ul class="nav navbar-nav mr-auto">
                    <li class="navbar-item"><a class="nav-link" class="a1" href="adiciona-evento.php"> Adiciona Evento</a></li>
                    <li class="navbar-item"><a class="nav-link" class="a2" href="evento-lista.php"> Lista de Eventos</a></li>
                    <li class="navbar-item"><a class="nav-link" class="a3" href="contato.php">Contato</a></li>

                    <li class="navbar-item"><a class="nav-link" class="a5" id="login" style="cursor: pointer;" onclick="modalLogin()">Login</a></li>
                   	
                    <li class="nav-item"><a id="deslogar" class="nav-link" href="logout.php">Deslogar</a></li>
                 </ul>

                 <div class="dropdown">
  					<button class="dropbtn">Criar Conta</button>
  					<div class="dropdown-content">
  						<a class="nav-link" class="a4" id="regProd" href="adiciona-produtor.php" ">Produtor Eventos</a>
					    <a class="nav-link" class="a5" id="registrar" onclick="modalUser()">Usuário Comum</a>
 					</div>
				</div>

                <form class="form-inline">
               		<input class="form-control ml-4 mr-3" class="a6" type="search" placeholder="Buscar...">
               		<button class="btn btn-dark" class="7" type="submit">OK</button>
                </form>

               <!--  <ul class="navbar-nav ml-auto">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggler="dropdown" id="navDrop">
                      Criar Conta
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">Facebook</a>
                        <a href="#" class="dropdown-item">Twitter</a>
                        <a href="#" class="dropdown-item">Instagram</a>
                    </div>
                  </li>
                </ul> -->

                



            </div>
        </div>
    </nav>

    <?php 
require_once 'mostra-alerta.php';
require_once 'funcoes-usuario.php';
mostraAlerta("success");
mostraAlerta("danger");
?>

  
    
<div class="" id="divmain">
    
        <div class="" id="divmainslider">

		          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						  <ol class="carousel-indicators">
						    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						  </ol>
				 	<div class="carousel-inner">
					    <div class="carousel-item active">
					    	<img class="d-block w-100" src="imagens/balada1.jpg" alt="First slide">
						</div>

						 <div class="carousel-item">
						      <img class="d-block w-100" src="imagens/balada2.jpg" alt="Second slide">
						 </div>

					    <div class="carousel-item">
					      <img class="d-block w-100" src="imagens/ls.jpg" alt="Third slide">
					    </div>

				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
        </div>




		<h1>Bem Vindo</h1><br/><br/>

		<?php if(estaLogado()) { ?>
			<p class="text-success">Você está logado como <?=usuarioLogado()?>
     			<a href="logout.php">Deslogar</a>   
      		</p>

		<?php }  else {?>


    <div id="modalL">
        <div class="modal-box">
           <div class="box-conteudo">

             <form action="login.php" method="POST" name="form1" id="form1">
                
                <tr><h1 class="my-4 text-monospace text-uppercase">Login</h1></tr>
                <tr>
                  <td><hr><p class="text-monospace"> Email:</p></hr></td>
                  <td><input class="form-control my-3" type="email" name="emailL" id="emailL"></td>
                </tr>

                <tr>
                  <td><p class="text-monospace">Senha: </p></td>
                  <td><input class="form-control" type="password" name="senhaL" id="senhaL"></td>
                </tr>
                
                  <tr>
                    <td><button class="btn btn-success my-4 text-monospace" onclick="return validar();" type="submit">Logar</button></td>
                  </tr>
                
                </form>
            </div>
            <div class="fechar">x</div>
        </div>
    </div>

 <script>
      function modalLogin(){
        document.getElementById("modalL").style.display = "block";
      }
      function modalUser(){
        document.getElementById("modalR").style.display = "block";
      }
      // function modalProdutor(){
      //   document.getElementById("modalP").style.display = "block";
      // }

  </script>

      <script type="text/javascript">
        $('.fechar, #modalL').click(function(e) {
          console.log("função fechar modal");
          if (e.target !== this)
            return;
          $('#modalL').fadeOut(500);
        });
      </script>

    <div id="modalR">
        <div class="modal-box">
           <div class="box-conteudo">
           <form action="register.php" method="post">
           
                <tr><h1 class="my-3 text-uppercase text-monospace">Usuário</h1></tr>
               
                <tr>
                  <td><hr><p class="text-monospace"> Digite o seu nome</p></hr></td>
                  <td><input class="form-control my-2" type="name" name="nomeR"></td>
                </tr>
                <tr>
                  <td><p class="text-monospace"> Digite um  e-mail valido:</p></td>
                  <td><input class="form-control my-2" type="email" name="emailR" id="emailR"></td>
                </tr>
                <tr>
                  <td><p class="text-monospace"> Digite uma senha:</p></td>
                  <td><input class="form-control my-2" type="password" name="senhaR"></td>
                </tr>
                <tr>
                  <td><p class="text-monospace"> Digite a senha novamente:</p></td>
                  <td><input class="form-control my-2" type="password" name="senhaR2"></td>
                </tr> 
              <tr>
                <td><button class="btn btn-success my-4 text-monospace" onsubmit="return validar1()" type="submit">Enviar
                </button></td>
              </tr>
            
            </form>
            </div>
            <div class="fechar">x</div>
        </div>
    </div>


    <script type="text/javascript">
      $('.fechar, #modalR').click(function(e) {
        console.log("função fechar modal");
        if (e.target !== this)
          return;
        $('#modalR').fadeOut(500);
      });
  </script>

  <!-- <div id="modalP">
        <div class="modal-box">
           <div class="box-conteudo">
           <form action="recebe-produtor.php" method="post">
           
                <tr><h1 class="my-3 text-uppercase text-monospace">Produtor</h1></tr>
               
	                <tr>
						<td><hr><p class="text-monospace">Nome:</p></td></td>
						<td><input class="form-control my-2" type="text" name="nome"></td>
					</tr>
					<tr>
						<td><p class="text-monospace">Sobrenome:</p></td>
						<td><input class="form-control my-2" type="text" name="sobrenome"></td>
					</tr>
					<tr>
						<td><p class="text-monospace">E-mail:</p></td>
						<td><input class="form-control my-2" type="email" name="email"></td>
					</tr>
					<tr>
						<td><p class="text-monospace">CEP:</p></td>
						<td><input class="form-control my-2" type="cep" name="cep"></td>
					</tr>
					<tr>
						<td><p class="text-monospace">CNPJ:</p></td>
						<td><input class="form-control my-2" type="text" name="cnpj"></td>
					</tr>
					<tr>
						<td><p class="text-monospace">CPF:</p></td>
						<td> <input class="form-control my-2" type="text" name="cpf"></td>
					</tr>
					<tr>
						<td><p class="text-monospace">Genêro</p></td>
						<td><input class="my-2" type="radio" value="0"  name="gnr">Masculino</td>
						<td><input class="my-2" type="radio" value="1"  name="gnr" checked>Feminino</td>
						<td><input class="my-2" type="radio" value="2"  name="gnr">Outro</td>
					</tr>
              <tr>
                <td><button class="btn btn-success my-4 text-monospace" onsubmit="return validar1()" type="submit">Enviar
                </button></td>
              </tr>
            
            </form>
            </div>
            <div class="fechar">x</div>
        </div>
    </div>

    <script type="text/javascript">
      $('.fechar, #modalP').click(function(e) {
        console.log("função fechar modal");
        if (e.target !== this)
          return;
        $('#modalP').fadeOut(500);
      });
  </script> -->
  

	<?php } ?>

	<div class="row">
		<div class="col-4">
			<p class="display-5">Ingressos Vendidos</p>
			<img src="imagens/icons82.png">
			<p class="display-4">100</p>
		</div>

		<div class="col-4">
			<p  class="display-5">Eventos Cadastrados</p>
			<img src="imagens/icons81.png">
			<p class="display-4">35</p>
		</div>

		<div class="col-4">
			<p class="display-5">Produtores de Eventos</p>
			<img src="imagens/icons8.png">
			<p class="display-4">15</p>
		</div>
	</div>

</div>
	
</body>
<?php require_once 'rodape.php'; ?>
</html>