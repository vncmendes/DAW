<?php
require_once 'conexao.php';
session_start();


function buscaUsuario ($conexao, $email, $senha) {
	$senhaMd5 = md5($senha);
	$email = mysqli_real_escape_string($conexao, $email);
	$query = "select * from usuarios where email = '{$email}' and senha = '{$senhaMd5}'";
	$resultado = mysqli_query($conexao, $query);
	$usuario = mysqli_fetch_assoc($resultado);
	return $usuario;
}

function inserirUsuario ($conexao, $nome, $email, $senha) {
	$senhaMd5 = md5($senha);
	$query = "insert into usuarios (nome, email, senha) values ('{$nome}', '{$email}', '{$senhaMd5}')";
	$resultado = mysqli_query($conexao, $query);

	return $resultado;
}


function estaLogado() {
	return isset($_SESSION["usuario_logado"]);
}

function verificaUsuario () {
	if (!estaLogado()) {
		$_SESSION["danger"] = "Acesso Negado !";
	header("Location: index.php?falhaSeg=true");
	die();
	}
}


function usuarioLogado() {
	return $_SESSION["usuario_logado"];
}

function logaUsuario($email) {
	$_SESSION["usuario_logado"] = $email;
}

function logOut() {
	session_destroy();
	session_start();
}

