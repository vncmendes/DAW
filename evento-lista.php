<?php

require_once 'cabecalho.php';
require_once 'funcoes-evento.php';
require_once 'funcoes-categoria.php';
require_once 'funcoes-usuario.php';
?> 


<div class="" id="divmain">
		
		<?php 
		mostraAlerta("success");
		mostraAlerta("danger");
		?>


<?php setlocale(LC_ALL,'pt_BR.UTF8');



 if (isset($_SESSION["success"])) { ?>
      <p class="alert-success"> <?= $_SESSION["success"]?></p>
  <?php 
      unset($_SESSION["success"]);
  } ?>

<?php 
$eventos = listaEvento($conexao);

foreach ($eventos as $evento) : 

$registra_eventocategorias = listaEventoCategoria($conexao, $evento['idevento']);
$registra_eventoestruturas = listaEventoEstrutura($conexao, $evento['idevento']);

?>
<div class="flex-container">
	<table class="table table-striped table-bordered"> 
			<tr>

				<td>Nome: <?= $evento['nome'] ?></td>
				<td>Data: <?= $evento['data'] ?> </td>
				<td>Hora Inicial: <?= $evento['horainicial'] ?></td>
				<td>Hora Final: <?= $evento['horafinal'] ?></td>
				<td>Localização: <?= $evento['localizacao'] ?></td>
				<td>Descrição: <?= substr($evento['descricao'], 0, 40) ?> </td>
				<td>Guarda Volumes: <?= $evento['gv'] ?> </td>
				<td>Ingresso: <?= $evento['ingresso'] ?> </td>
				<td> Categoria: <?php 
					foreach ($registra_eventocategorias as $categoria) : 
							echo $categoria['nome']; ?> <br>
					<?php  endforeach; ?> 
				</td>
					
				<td>
					 Estrutura: <?php 
					foreach ($registra_eventoestruturas as $estrutura) : 
							echo $estrutura['nome']; ?> <br>
					<?php endforeach; ?>
				 	
				 </td>

				 
				<td><img class="foto" src="imagens/<?=$evento['arquivo']?>"></td>
				<td><a class="btn btn-success" href="evento-altera-form.php?id=<?=$evento['idevento']?>">Alterar</a></td>
				<td>

					<form action="remove-evento.php" method="POST">
						<input type="hidden" name="id" value="<?=$evento['idevento']?>">
						<button type="submit" class="btn btn-danger text-danger">Remover</button>
					</form>
				</td>
			
			</tr>
	</table>
</div>

	
</div>


<?php endforeach;

require_once 'rodape.php'; ?>