<?php 
require_once 'funcoes-evento.php';
require_once 'funcoes-usuario.php';

verificaUsuario();

$validar = false;


$nome = $_POST['nome'];
$sobrenome = $_POST['sobrenome'];
$email = $_POST['email'];
$cep = $_POST['cep'];
$cnpj = $_POST['cnpj'];
$cpf = $_POST['cpf'];
$gnr = $_POST['gnr'];
$senha = $_POST['senha'];
$senha2 = $_POST['senha2'];

if ($nome == "" || $sobrenome == "" || $email == "" || $cep == "" || $cnpj == "" || $cpf == "" || $gnr == "" || $senha == "" || $senha2 == "") {
	$_SESSION["danger"] = "Preencha todos os campos";
	header("Location: adiciona-produtor.php");
	
}

if ($senha != $senha2) {
	$_SESSION["danger"] = "Digite a mesma senha !";
	header("Location: adiciona-produtor.php");
}

else {
	$validar = true;
}

if ($validar == true) {

if (inserirAdm ($nome, $sobrenome, $email, $cep, $cnpj, $cpf, $gnr, $senha )) { ?>
	<p class="text-success">Produtor <?= $nome ?> foi inserido com Sucesso</p> 
<?php }  

else { 
	$msg = mysqli_error($conexao); ?> 
	<p class="text-danger">Produtor <?= $nome ?> nao foi Adicionado. <?= $msg ?> </p> <?php
}

// if ($sobrescrever == "não" && file_exists("imagens/$arquivo"))
// 	die("Arquivo já existe");

// if ($limitar_tamanho =="não" && ($tamanho_arquivo > $tamanho_bytes))
// 	die("Arquivo deve ter no máximo $tamanho_bytes bytes");

// $ext = strrchr($arquivo, '.');

// if (($limitar_ext == "sim") && !in_array ($ext, $extensoes_validas))
// 	die("Extensão de arquivo inválida para upload");


// if (!empty($arquivo)) {
// 	if (move_uploaded_file($arquivo_temp, "imagens/$arquivo")) {
// 		echo "Upload do arquivo: ".$arquivo. " conclúido com sucesso";
// 	}
// 	else {
// 		die("Falha no Erro");
// 	}
	
// }
// 	else {
// 		die("Selecione o arquivo a ser enviado");
// 	}
// }


require_once 'rodape.php'; ?>