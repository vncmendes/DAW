<?php 
require_once 'funcoes-evento.php';
require_once 'cabecalho.php';
require_once 'funcoes-categoria.php';
require_once 'funcoes-usuario.php';

verificaUsuario();
$id = $_GET['id'];
$evento = buscaEvento($conexao, $id);
$categorias = listaCategoria($conexao);
$estruturas = listaEstrutura($conexao);
$catEventoCategoria = listaEC($conexao, $id);
$catEventoEstrutura = listaEE($conexao, $id);
?>

	<h1>Alterando Evento</h1>
<form action="altera-evento.php" method="POST" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?=$evento['idevento']?>">
	<table class="table">
	<tr>
		<td>Nome do Evento:</td>
		<td><input class="form-control type="text" name="nome" value="<?=$evento['nome']?>"></td>
	</tr>
	<tr>
		<td>Data:</td>
		<td><input class="form-control" type="date" name="data" value="<?=$evento['data']?>"></td>
	</tr>
	<tr>
		<td>Hora Inicial:</td>
		<td><input class="form-control" type="time" name="horai" value="<?=$evento['horainicial']?>"></td>
</tr>
<tr>
	<td>Hora Final:</td>
	<td><input class="form-control" type="time" name="horaf" value="<?=$evento['horafinal']?>"></td>
</tr>	
	<tr>
		<td>Localização:</td>
		<td><input class="form-control type="text" name="localizacao" value="<?=$evento['localizacao']?>"></td>
	</tr>
	<tr>
		<td>Descrição:</td>
		<td> <textarea class="form-control" name="descricao"><?=$evento['descricao']?></textarea></td>
	</tr>
	<tr>
	<?php 	if($evento['gv']==0)  {
			    	
			   $sim="";
			   $nao="checked";

			  }
			else {
				$sim="checked";
			    $nao="";
			   }

			?>
		<td>Guarda-Volumes</td>
		<td><input type="radio" value="1"  name="gv" <?=$sim?>>Sim</td>
		<td><input type="radio" value="0"  name="gv" <?=$nao?>>Não</td>
	</tr>
	<tr>
		<td>Ingresso:</td>
		<td><input class="form-control" type="number" name="ingresso" value="<?=$evento['ingresso']?>"></td>
	</tr>
	
	
	<tr>
		<td id="ce">Categoria<br>
			
			<?php foreach ($categorias as $categoria) : 

				if(in_array($categoria['idcategoria'],$catEventoCategoria)) {
			    	$checked="checked";
			    }
			    else {
			    	$checked="";
			    } ?>

			<input type="checkbox" name="categoria[]" value="<?=$categoria['idcategoria']?>" <?=$checked?> > <?=$categoria['nome']?><br>
			<?php endforeach; ?>
		</td>	
		<td>
			
		</td>

	
		<td>Estrutura<br>
		
			<?php foreach ($estruturas as $estrutura) :

				if(in_array($estrutura['idestrutura'],$catEventoEstrutura)) {
			    	$checked="checked";
			    }
			    else {
			    	$checked="";
			    } ?>

				<input type="checkbox" name="estrutura[]" value="<?=$estrutura['idestrutura']?>" <?=$checked?> ><?=$estrutura['nome']?><br>
			<?php endforeach; ?>
		</td>

	
	</tr>
		<td>Foto:</td>
		<td><input type="file" name="arquivo"></td>
	
		<input type="hidden" name="max_file_size" value="200000">
		<td><input class="btn btn-success" type="submit" value="Alterar"></td>
		<td><a class="btn" href="index.php">Voltar</a></td>
</table>

<?php require_once 'rodape.php'; ?>
